# microtick_examples

## Step 1 - Set up a local network

These examples assume you have a working Microtick node, and a running API server.
To run a local API server, set up a local testnet using the following commands and 
the latest testnet Microtick [binary release](https://microtick.com/releases/testnet):

```
$ export MTROOT=$HOME/mylocaltestnet
$ mtd init local --chain-id localnet
```

Edit the file ./local/mtd/config/genesis.json and add some markets and durations:

- Set app_state.microtick.markets => [{name:"XBTUSD",description:"Crypto - Bitcoin"}]
- Set app_state.microtick.durations => [{name:"5minute",seconds:300},{name:"15minute",seconds:900},{name:"1hour",seconds:3600}]

Now, generate a default validator and a default bank account, and run the node:

```
$ mtcli config chain-id localnet
$ mtcli config keyring-backend file
$ mtcli keys add validator
$ mtcli keys add bank
$ mtd add-genesis-account validator 1000000000000stake --keyring-backend file
$ mtd add-genesis-account bank 1000000000000udai --keyring-backend file
$ mtd gentx --name validator --amount 1000000000000stake --keyring-backend file
$ mtd collect-gentxs
$ mtd start
```

At this point your node should be running. Switch to another window and run a 
Tendermint rest-server:

```
$ export MTROOT=$HOME/mylocaltestnet
$ mtcli rest-server
```

Now you can run a local instance of the Microtick API server. Clone the mtapi repository:

```
$ git clone https://gitlab.com/microtick/mtapi.git
$ cd mtapi/server
$ cp config-example.json config.json
```

Edit the config.json and set "use_database" to false.  Then run the server:

```
$ yarn install
$ node server
```

## Step 2 - Run the example

Once the network is set up and running with a node, rest server and API, try out
the example in this directory:

```
$ yarn install
$ node demo_api
```

## How to fund your account

The first time through the demo, you account balance will be 0.  To fund your
account, copy the address and run the following commands:

```
$ export MTROOT=$HOME/mylocaltestnet
$ mtcli tx send bank 10000000udai -y
```

Then, restart the demo.
