const Microtick = require('microtick')
const inquirer = require('inquirer')

// You need to have a running microtick node and API server to utilize the API
const server_url = "ws://localhost:1320"
const api = new Microtick(server_url)

class MarketMaker {
  
  constructor() {
    // DO NOT USE THESE. Generate your own 12 word mnemonic here: https://iancoleman.io/bip39/
    this.mnemonic = "super surround arrest wool paper supply confirm strong near boring fancy sponsor".split(" ")
    
    this.market = "XBTUSD"
    this.duration = "1hour"
  }
  
  async init() {
    this.wallet = await api.init(this.mnemonic)
    console.log("My address = " + this.wallet.acct)
    this.markets = api.getMarkets()
    this.durations = api.getDurations()
  }
    
  async main() {
    var params
    do {
      // You will also need an account balance to do anything useful. See the README
      const info = await api.getAccountInfo(this.wallet.acct)
      console.log("***")
      console.log("Account balance = " + info.balance + " dai")
      console.log("Num quotes = " + info.activeQuotes.length)
      console.log("Num trades = " + info.activeTrades.length)
      console.log("***")
    
      const choices = ["Quotes"]
      if (info.activeTrades.length > 0) {
        choices.push("Trades")
      }
      choices.push("Exit")
      params = await inquirer.prompt(
        { name: "action", type: "list", message: "Main menu", prefix: "===", suffix: " ===", choices: choices }
      )
      if (params.action === "Quotes") {
        await this.quotes()
      }
      if (params.action === "Trades") {
        await this.trades()
      }
    } while (params.action !== "Exit")
  }
  
  async quotes() {
    const info = await api.getAccountInfo(this.wallet.acct)
    const choices = [ "Create Quote" ].concat(info.activeQuotes)
    var params = await inquirer.prompt(
      { name: "action", type: "list", message: "Quotes", prefix: "---", suffix: " ---", choices: choices }
    )
    if (params.action === "Create Quote") {
      await this.createQuote()
    } else {
      await this.quote(params.action)
    }
  }
  
  async createQuote() {
    const params = await inquirer.prompt([
      { name: "market", type: "list", prefix: "", message: "Market", choices: this.markets.map(m => m.name) },
      { name: "duration", type: "list", prefix: "", message: "Duration", choices: this.durations.map(d => d.name) },
      { name: "backing", type: "number", prefix: "", message: "Backing" },
      { name: "spot", type: "number", prefix: "", message: "Spot" },
      { name: "premium", type: "number", prefix: "", message: "Premium" }
    ])
    try {
      console.log("Creating quote...")
      const res = await api.createQuote(params.market, params.duration, params.backing + "dai",
        params.spot + "spot", params.premium + "premium")
      console.log("Success: " + JSON.stringify(res))
    } catch (err) {
      console.log("Error: " + err.message)
    }
  }
  
  async quote(id) {
    const quote = await api.getLiveQuote(id)
    console.log("Quote: " + id)
    console.log("Market: " + quote.market)
    console.log("Duration: " + quote.duration)
    console.log("Spot: " + quote.spot)
    console.log("Premium: " + quote.premium)
    console.log("Price as call: " + quote.premiumAsCall)
    console.log("Price as put: " + quote.premiumAsPut)
    var answers = await inquirer.prompt({
      type: "list",
      name: "action",
      message: "Quote Action",
      choices: [ "Update Quote", "Cancel Quote", "Exit" ]
    })
    if (answers.action === "Update Quote") {
      await this.updateQuote(id)
    }
    if (answers.action === "Cancel Quote") {
      await this.cancelQuote(id)
    }
  }
  
  async updateQuote(id) {
    const params = await inquirer.prompt([
      { name: "spot", type: "number", prefix: "", message: "Spot", default: 0 },
      { name: "premium", type: "number", prefix: "", message: "Premium", default: 0 }
    ])
    try {
      console.log("Updating quote...")
      const res = await api.updateQuote(id, params.spot + "spot", params.premium + "premium")
      console.log("Success: " + JSON.stringify(res))
    } catch (err) {
      console.log("Error: " + err.message)
    }
  }
  
  async cancelQuote(id) {
    try {
      console.log("Canceling quote...")
      const res = await api.cancelQuote(id)
      console.log("Success: " + JSON.stringify(res))
    } catch (err) {
      console.log("Error: " + err.message)
    }
  }
  
  async trades() {
    const info = await api.getAccountInfo(this.wallet.acct)
    var params = await inquirer.prompt(
      { name: "action", type: "list", message: "Quotes", prefix: "---", suffix: " ---", choices: info.activeTrades }
    )
    await this.trade(params.action)
  }
  
  async trade(id) {
    try {
      const trade = await api.getLiveTrade(id)
      console.log("Trade: " + id)
      console.log("Market: " + trade.market)
      console.log("Duration: " + trade.duration)
      console.log("Type: " + trade.option)
      console.log("Strike: " + trade.strike)
      console.log("Current value: " + trade.currentValue)
      console.log("Expiration: " + new Date(trade.expiration).toLocaleString())
    } catch (err) {
      console.log("Error: " + err.message)
    }
  }
  
}

async function main() {
  const marketmaker = new MarketMaker()
  await marketmaker.init()
  await marketmaker.main()
  process.exit()
}

main()

